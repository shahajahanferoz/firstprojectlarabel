<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashBoardController;
use App\Http\Controllers\PublicController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home',[PublicController::class, 'home'])->name('hom');
Route::get('/about',[PublicController::class, 'about'])->name('about');
Route::get('/login',[AuthController::class, 'login'])->name('login');
Route::get('/registration',[AuthController::class, 'registration'])->name('reg');
Route::get('/dashboard',[DashBoardController::class, 'dashboard'])->name('dashboard');

Route::prefix('admin')->group(function () {
    Route::get('/',[DashBoardController::class, 'home']);
    Route::get('/table',[DashBoardController::class, 'table']);
});