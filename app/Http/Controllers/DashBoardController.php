<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashBoardController extends Controller
{
    public function dashboard(){
        return view('/dashboard');
    }

    public function home(){
        return view('/admin.home');
    }

    public function table(){
        return view('/admin.table');
    }

}
